<?php

namespace Drupal\od_log_prune\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form definition for log pruning.
 *
 * @author h2b
 */
class LogPruneConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames () {
    return [
        'od_log_prune.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId () {
    return 'log_prune_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm (array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('od_log_prune.settings');
    $form['age_limit_days'] = [
        '#type' => 'number',
        '#min' => 0,
        '#title' => $this->t('Age limit:'),
        '#description' => $this->t(
            'Number of days a log entry shall be kept. ' .
            '0 means forever (regarding to this module).'),
        '#field_suffix' => $this->t('days'),
        '#default_value' => $config->get('age_limit_days'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm (array &$form, FormStateInterface $form_state){
    $config = $this->config('od_log_prune.settings');
    $config->set('age_limit_days', $form_state->getValue('age_limit_days'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

}
